# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2021-09-29 05:43
from __future__ import unicode_literals

from django.db import migrations, models





class Migration(migrations.Migration):

    dependencies = [
        ('socialfeedsparser', '0006_auto_20210929_0543'),
    ]

    operations = [
        migrations.AddField(
            model_name='channel',
            name='page_access_token',
            field=models.TextField(null=True, blank=True, verbose_name="Page Access Token"),
            preserve_default=False,
        )
    ]

