from django.core.management.base import BaseCommand
from django.utils.timezone import now

from socialfeedsparser.models import Channel


class Command(BaseCommand):
    """
    Command used to get messages for the instances stored in models.Channel.

    usage:

        python manage.py collect_info_about_us
    """
    help = 'Collect messages from social services'

    def add_arguments(self, parser):
        parser.add_argument(
            '--source',
            dest='source',
            help='Collect feed for a specific source',
        )

    def handle(self, *args, **options):
        source = options.get('source')
        channels = Channel.objects.published()

        if source:
            channels = channels.filter(source=source)
        channels = [_ for _ in channels if _.can_update()]

        for channel in channels:
            self.stdout.write(u'Processing source: "%s"' % channel)
            sc = channel.source_class(channel=channel)
            sc.collect_messages()
            channel.updated = now()
            channel.save()
