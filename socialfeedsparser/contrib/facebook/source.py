import facebook as fbsdk
from django.conf import settings
import requests

from socialfeedsparser.contrib.parsers import ChannelParser, PostParser
from socialfeedsparser.settings import SOCIALFEEDSPARSER_TIMEOUT
try:
    from urllib2 import urlopen
    from urllib import urlencode
except (ImportError):  # for python >= 3.4
    from urllib.request import urlopen
    from urllib.parse import urlencode


class FacebookSource(ChannelParser):
    """
    Collect class for Facebook.
    """

    name = 'Facebook'
    slug = 'facebook'

    def get_messages_user(self, feed_id, since, access_token=None):
        """
        Return posts from user or page feed.

        :param feed_id: id of the page or user to parse.
        :param access_token: access token to use for authentication.
        """
        if not access_token:
            print('Missing access token for Facebook feed: {}'.format(feed_id))
            return []
        try:
            ret = self.get_api(access_token).get_connections(feed_id.encode('utf-8'), 'feed', since=since)['data']
        except(fbsdk.GraphAPIError):
            ret = self.get_api(access_token).get_connections(feed_id, 'feed', since=since)['data']

        return ret

    def get_messages_search(self, search):
        """
        Return posts by search param.
        THIS API FEATURE HAS BEEN DISABLED BY FACEBOOK

        :param search: search string to search for on twitter.
        :type item: str
        """
        return {}

    def get_api(self, access_token):
        """
        Return authenticated connections with fb.
        """
        api = fbsdk.GraphAPI(access_token, timeout=float(SOCIALFEEDSPARSER_TIMEOUT))
        return api

    def prepare_message(self, message):
        """
        Convert tweets to standard message.

        :param message: message entry to convert.
        :type item: dict
        """
        l = 'http://www.facebook.com/permalink.php?id=%s&v=wall&story_fbid=%s' \
            % (message['id'], message['id'].split('_')[1])
        return PostParser(
            uid=message['id'],
            author=message.get('name', ''),
            author_uid=message['id'],
            content=message.get('message', '') or message.get('description', ''),
            date=message['created_time'],
            image=message.get('picture', None),
            link=l
        )


def validate_page_token(query, page_token):
    page_id = None
    if page_token:
        url = "https://graph.facebook.com/v13.0/{page_id}?fields=id,posts&access_token={token}".format(
            page_id=query,
            token=page_token)
        response = requests.get(url).json()
        page_id = response.get('id')

    return not not page_id


def get_facebook_page_token(query, short_living_token):
    # step 1 get user long living token
    url = "https://graph.facebook.com/oauth/access_token?grant_type=" \
          "fb_exchange_token&client_id={client}&client_secret={secret}&fb_exchange_token={token}".format(
            client=settings.SOCIALFEEDSPARSER_FACEBOOK_CLIENT_ID,
            secret=settings.SOCIALFEEDSPARSER_FACEBOOK_CLIENT_SECRET,
            token=short_living_token
          )

    response = requests.get(url).json()
    long_living_token = response.get('access_token')

    if not long_living_token:
        return None

    # step 2 get page token without expiration date

    url = "https://graph.facebook.com/v13.0/{page_id}?fields=access_token&access_token={token}".format(
        token=long_living_token, page_id=query)

    response = requests.get(url).json()
    return response.get('access_token')
