from django import forms
from django.contrib import admin
from django.template.defaultfilters import truncatewords
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _

from .contrib.facebook.source import get_facebook_page_token, validate_page_token
from .models import Post, Channel


def get_messages(modeladmin, request, queryset):
    """
    Collects messages from selected sources.
    """
    for source in queryset:
        sc = source.source_class(channel=source)
        sc.collect_messages()
        source.updated = now()
        source.save()


get_messages.short_description = _('Get Messages from selected sources')


class ChannelAdminForm(forms.ModelForm):
    def clean(self):
        cleaned_data = super(ChannelAdminForm, self).clean()
        page_access_token = cleaned_data.get('page_access_token')
        query = cleaned_data['query']

        if query and page_access_token and page_access_token != self.instance.page_access_token and \
                cleaned_data['source'] == 'facebook':
            page_token = get_facebook_page_token(query, page_access_token)
            if page_token and validate_page_token(query, page_token):
                cleaned_data['page_access_token'] = page_token
            else:
                self.add_error('page_access_token', 'Invalid Access token, Please use the login button '
                                                    'again and allow access to the corresponding page')
        return cleaned_data

    class Meta:
        model = Channel
        fields = '__all__'


class ChannelAdmin(admin.ModelAdmin):
    """
    Admin class for the Channel model.
    """
    list_display = ('query', 'name', 'source', 'query_type', 'updated', 'is_active', 'show_linkedin_token_renew_link')
    list_filter = ('query', 'source', 'query_type', 'updated', 'is_active')
    exclude = ('user_secret', 'user_token',)
    actions = [get_messages]
    radio_fields = {"query_type": admin.HORIZONTAL}
    form = ChannelAdminForm

    def show_linkedin_token_renew_link(self, obj):
        return '<a href="%s">%s</a>' % (obj.token_renew_link, _('Click to renew')) if obj.source == 'linkedin' else ''
    show_linkedin_token_renew_link.allow_tags = True
    show_linkedin_token_renew_link.short_description = _('Renew Token')


class PostAdmin(admin.ModelAdmin):
    """
    Admin class for the Post model.
    """
    list_display = ('channel', 'author', 'content_admin', 'date',
                    'is_active', 'order')
    list_filter = ('is_active', 'channel')
    list_editable = ('is_active', 'order', 'author', 'date')

    def content_admin(self, obj):
        return truncatewords(obj.content, 20)
    content_admin.short_description = _('Post content')


admin.site.register(Channel, ChannelAdmin)
admin.site.register(Post, PostAdmin)
